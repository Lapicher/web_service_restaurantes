var config = require('./config');
var express= require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
global.conexion = require('./lib/conexion.js');


// Usar los endpoints de la carpeta "api"
var endpoints_verifica = require('./api/index.js');
app.use('/', endpoints_verifica);

var sucursal = require('./api/sucursal.js');
app.use('/', sucursal);


var mesa = require('./api/mesa.js');
app.use('/', mesa);

var categoria = require('./api/categoria.js');
app.use('/', categoria);

var platillo = require('./api/platillo.js');
app.use('/', platillo);

var grupo = require('./api/grupo.js');
app.use('/', grupo);

var pago = require('./api/pago.js');
app.use('/', pago);

var usuario = require('./api/usuario.js');
app.use('/', usuario);

var rol = require('./api/rol.js');
app.use('/', rol);


app.listen(config.puerto,function(){
       console.log("Servidor corriendo en el puerto "+config.puerto);
});
