/**
 * Created by JANELI on 08/05/2017.
 */
var mysql = require('mysql');
var config = require('../config');

//funcion

var connection = mysql.createConnection(config.mysql);

connection.connect(function(error){
    if(error){
        throw error;
    }else{
        console.log('Conexion correcta.');
    }
});

//connection.end();

module.exports = connection;